
# coding: utf-8

# In[1]:


import numpy as np
import csv
import sys

path = "data/"
start_train = "_train["
file_type_end = "].csv"
i = 0
n_folds = 1
csv_x_train = path + "x" + start_train + str(i) + file_type_end
csv_y_train = path + "y" + start_train + str(i) + file_type_end
x_train = [[] for _ in iter(range(n_folds))]
y_train = [[] for _ in iter(range(n_folds))]
x_train[i] = np.array(list(csv.reader(open(csv_x_train)))).astype(np.float)
y_train[i] = np.array(list(csv.reader(open(csv_y_train)))).astype(np.float)


# In[2]:


print ("np.shape(x_train[i]): {}".format(np.shape(x_train[i])))
sys.stdout.write("np.shape(x_train[i]): {}".format(np.shape(x_train[i])))

from sklearn import preprocessing
import math
import itertools

fancy_PCA_add = 1
verbose = 2
if fancy_PCA_add > 0:
    sys.stdout.write(("doing Facny PCA\n"))
    if verbose>0:
        sys.stdout.write(("\nadding fancy_PCA_add: {}\n".format(fancy_PCA_add)))
    min_max_scaler_cancer = preprocessing.MinMaxScaler()
    min_max_scaler_healthy = preprocessing.MinMaxScaler()
    idx_cancer = np.where(np.array(y_train[i] == [1, 0]).astype(int)[:,0])[0]
    idx_healthy = np.where(np.array(y_train[i] == [0, 1]).astype(int)[:,0])[0]
    x_cancer = x_train[i][idx_cancer]
    x_healthy = x_train[i][idx_healthy]
    shape_x_train = np.shape(x_train[i])
    
    # Making space in memory
    del y_train
    #np.savetxt("x_train[i][0].csv", x_train[i][0], delimiter=",")
    del x_train

    scaled_data_cancer = min_max_scaler_cancer.fit_transform(x_cancer)
    scaled_data_healthy = min_max_scaler_healthy.fit_transform(x_healthy)
   
    m_cancer = scaled_data_cancer.mean(axis=0)   
    m_healthy = scaled_data_healthy.mean(axis=0)
    scaled_data_cancer = scaled_data_cancer - m_cancer
    scaled_data_healthy = scaled_data_healthy - m_healthy
    
    R_cancer = np.cov(scaled_data_cancer, rowvar=False)
    R_healthy = np.cov(scaled_data_healthy, rowvar=False)
    sys.stdout.write(("before svd\n"))
    u, s, vh = np.linalg.svd(R_cancer, full_matrices=False)
    sys.stdout.write(("1/4 svd\n"))
    u2, s2, vh2 = np.linalg.svd(R_healthy, full_matrices=False)
    sys.stdout.write(("2/4 svd\n"))
    u_x, s_x, vh_x = np.linalg.svd(scaled_data_cancer, full_matrices=False)
    sys.stdout.write(("3/4 svd\n"))
    u_x2, s_x2, vh_x2 = np.linalg.svd(scaled_data_healthy, full_matrices=False)
    fact_cancer = np.float(scaled_data_cancer.shape[0] - 1)
    fact_healthy = np.float(scaled_data_healthy.shape[0] - 1)
    sys.stdout.write(("after svd\n"))
    
    
    #y_train_add = np.zeros(np.shape(y_train[i])) + y_train[i]
    for k in iter(range(fancy_PCA_add)):
        x_train_new = np.zeros(shape_x_train)
        f = 0.7
        f2 = 0
        temp = np.array([np.random.uniform(f2, 3*math.sqrt(s[j]))for j in iter(range(len(s)))]) #  np.random.uniform(f2, f)
        temp2 = np.array([np.random.uniform(f2, 3*math.sqrt(s2[j])) for j in iter(range(len(s2)))])
        evals = np.array((s + temp*s)**0.5)*math.sqrt(fact_cancer) #+ temp*s
        evals2 = np.array((s2 + temp*s)**0.5)*math.sqrt(fact_healthy)
        new_scaled_data_cancer = np.array(np.dot(u_x* evals[:len(s_x)], vh_x)) #evals[:len(s_x)]
        new_scaled_data_healthy = np.array(np.dot(u_x2* evals2[:len(s_x2)], vh_x2))
        
        new_scaled_data_cancer = new_scaled_data_cancer + m_cancer
        new_scaled_data_healthy = new_scaled_data_healthy + m_healthy
        
        fancy_PCA_data_cancer = np.array(min_max_scaler_cancer.inverse_transform(new_scaled_data_cancer)) #[:,:len(evals)]
        fancy_PCA_data_healthy = np.array(min_max_scaler_healthy.inverse_transform(new_scaled_data_healthy))

        x_train_new[idx_cancer] = fancy_PCA_data_cancer
        x_train_new[idx_healthy] = fancy_PCA_data_healthy
        #if k == 0:
            #print ("are you ready(allclose(x_train[i], x_train_new))?: {}".format(np.allclose(x_train[i], x_train_new)))

        #print ("Before: x_train[i][-1, :10]: {}".format(x_train[i][-1, :10]))
        #x_train[i] = np.array(list(itertools.chain(x_train[i], x_train_new)))
        #y_train[i] = np.array(list(itertools.chain(y_train[i], y_train_add)))
        #print ("After: x_train[i][-1, :10]: {}".format(x_train[i][-1, :10]))
        #print ("x_train_new[-1, :10]: {}".format(x_train_new[-1, :10]))

        #print ("x_train[i][0,:10]: {}".format(x_train[i][0, :10]))
        #print ("x_train_new[0,:10]: {}".format(x_train_new[0, :10]))

        #print ("np.shape(x_train[i][0]): {}".format(np.shape(x_train[i][0])))
        #print ("np.shape(x_train_new[0]): {}".format(np.shape(x_train_new[0])))
        #print ("\nwriting to csv:")
        
        np.savetxt("x_train_new[0].csv", x_train_new[0], delimiter=",")
        
# Adding the coefficients randmly
# ind = np.random.rand(x_train[i].shape[0]).argsort()
# x_train[i] = x_train[i][ind]
# y_train[i] = y_train[i][ind]

