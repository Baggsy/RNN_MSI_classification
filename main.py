
# coding: utf-8

# In[1]:


#!python

from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.layers import Dense, Activation, LSTM, Bidirectional
from tensorflow.python.keras import metrics
from helper_funct import *
import time
import os

# TODO: gradient back propagation
#       freeze weights and load them for more layers. Train stack
#       use 100 units to find best method.
#       Camelyon17 images. Use CNN network
#       combine image and MSI data. Image captioning
#       stateful = true, or return sequences in last layer



# In[2]:



# HYPER Parameters
mini_batch_size = 32
embedding_size = 8
learning_rate = 0.005
epoch_train = 300  # maximum repetitions
optimizer_str = "RMSprop"
optimizer = keras.optimizers.RMSprop(lr=learning_rate) # keras.optimizers.Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0, amsgrad=True) #
metrics = ['accuracy', 'mae']
bias_init = 'he_normal'  # It draws samples from a truncated normal distribution centered on 0 with stddev = sqrt(2 / fan_in) where  fan_in is the number of input units in the weight tensor.
kernel_init = 'he_normal'
weight_init = 'he_normal'
use_bias = True
stateful=False
verbose = 0


# Model parameters
units = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]
layers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15]
lstm_type = ['LSTM', 'Bidirectional']
activation = 'softmax'
loss_function = 'binary_crossentropy'
merge_mode = ['ave', 'concat', 'sum']

units = [10]
layers = [1, 2, 3, 4, 5]
lstm_type = ['Bidirectional']
merge_mode = 'sum'

file = open("results.txt", "a")
file.write("\n\nmini_batch_size: {} learning_rate: {} optimizer: {}".format(mini_batch_size, learning_rate, optimizer_str))
try:

    # Data specific parameters
    n_sets = 8
    n_folds = 4
    num_classes = 2
    time_steps = 1


    # In[3]:


    # initialing the output array
    balanced_accuracy = np.zeros((len(units), len(layers), len(lstm_type)))
    run_time2 = np.zeros((len(units), len(layers), len(lstm_type)))

    # Reading the data from the read_data function
    x_train, x_test, x_val, y_val, y_train, y_test = read_data(n_folds)

    # in_shape defines the input shape of the LSTM modules
    in_shape = len(x_train[0][0][0])  # data length variable for the input tensor


    # In[4]:



    start_time = time.time()
    for unit in units:
        for n_layers in layers:
            for type in lstm_type:
                print "unit: ", unit
                print "n_layers: ", n_layers
                print "type: ", type
                start_time2 = time.time()
                model = Sequential()
                # Adding the LSTM layers or the Bidirectional LSTM modules
                if type == 'LSTM':
                    if n_layers > 1:
                        model.add(LSTM(units=unit, batch_input_shape=(mini_batch_size, time_steps, in_shape), return_sequences=True, stateful=stateful,
                                       bias_initializer=bias_init, kernel_initializer=kernel_init,
                                       recurrent_initializer=weight_init, use_bias=use_bias, name="LSTM_1"))
                        for j in range(2, n_layers):
                            model.add(LSTM(units=unit, return_sequences=True, bias_initializer=bias_init, stateful=stateful,
                                           kernel_initializer=kernel_init, recurrent_initializer=weight_init,
                                           use_bias=use_bias, name = "LSTM_{}".format(j)))
                        model.add(LSTM(units=unit, bias_initializer=bias_init, kernel_initializer=kernel_init, stateful=stateful,
                                       recurrent_initializer=weight_init, use_bias=use_bias, name="LSTM_{}".format(n_layers)))
                    else:
                        model.add(LSTM(units=unit, batch_input_shape=(mini_batch_size, time_steps, in_shape), bias_initializer=bias_init, stateful=stateful,
                                       kernel_initializer=kernel_init, recurrent_initializer=weight_init,
                                       use_bias=use_bias, name="LSTM_1"))
                else:
                    if n_layers > 1:
                        model.add(Bidirectional(LSTM(units=unit, return_sequences=True, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init,
                                                     recurrent_initializer=weight_init), batch_input_shape=(mini_batch_size, time_steps, in_shape),
                                                merge_mode=merge_mode, name="Bid_{}_1".format(merge_mode)))
                        for j in range(2, n_layers):
                            model.add(Bidirectional(LSTM(units=unit, return_sequences=True, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init, recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_{}".format(merge_mode, j)))
                        model.add(Bidirectional(LSTM(units=unit, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init, recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_{}".format(merge_mode, n_layers)))
                    else:
                        model.add(Bidirectional(LSTM(units=unit, bias_initializer=bias_init, stateful=stateful, kernel_initializer=kernel_init, recurrent_initializer=weight_init), input_shape=(time_steps, in_shape), merge_mode=merge_mode, name="Bid_{}_1".format(merge_mode)))
                        
                i = n_layers - 1
                layers_to_load = 0
                isloaded = False
                while i > 0 and not isloaded:
                    file_name = "models_saved/Weights_layers={}_Type={}_units={}_Set=0.h5".format(i, type, unit)
                    if os.path.isfile(file_name):
                        isloaded = True
                        layers_to_load = i
                    i -= 1
                
                if isloaded:
                   for layer in model.layers[:i+1]:
                       layer.trainable = False

                #for layer in model.layers:
                    #print(layer, layer.trainable)
                    #print(layer.name)

                # Adding the rest of the network's components
                model.add(Dense(units=num_classes, name="Dense_layers_{}".format(n_layers)))
                model.add(Activation(activation=activation))
                # model.compile(loss=loss_function, optimizer=optimizer, metrics=metrics)
                model.summary()

                if (n_layers > 1):
                    shuffle = False
                else :
                    shuffle = True

                # training of the model
                bal_accuracy = train_model(x_train, y_train, x_val, y_val, epoch_train, mini_batch_size, shuffle,
                                                x_test, y_test, model, n_folds, learning_rate, optimizer, verbose,
                                                loss_function, metrics, isloaded, n_layers, layers_to_load, type, unit)

                # Saving the balanced accuracy over the 4 folds
                balanced_accuracy[units.index(unit), layers.index(n_layers), lstm_type.index(type)] = bal_accuracy
                run_time2[units.index(unit), layers.index(n_layers), lstm_type.index(type)] = time.time() - start_time2
                print "run time of units {} n_layers {} of type {} : {}\n\n---------------------\n\n".format(unit, n_layers, type, run_time2[units.index(unit), layers.index(n_layers), lstm_type.index(type)])

                file.write(" units: {} n_layers: {} type: {} balanced accuracy: {}\n".format(unit, n_layers, type, bal_accuracy))

                del model


    run_time = time.time() - start_time

    print ""
    print "balanced_accuracy: "
    print balanced_accuracy
    print "run_time2: "
    print run_time2
    # print ""
    print "Total run time: ", run_time
    file.write("Balanced accuracy: {}".format(balanced_accuracy))
    file.write("times: {}\n\n".format(run_time2))
except (KeyboardInterrupt, SystemExit):
        print "KeyboardInterrupt error" #raise
except Exception as e:
    raise
    file.write("\nError: {}\n\n".format(str(e)))
    print "\nError: {}\n\n".format(str(e))

file.write("---------------------\n\n".format(str(e)))
file.close()

