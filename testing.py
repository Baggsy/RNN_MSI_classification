from pylab import *
import csv
import math
import matplotlib.pyplot as plt


csv_x_train = 'data/x_train[0].csv'

x = np.array(list(csv.reader(open(csv_x_train)))).astype(np.float)
x = x - x.mean(axis=0)
R_cancer = np.cov(x, rowvar=False)

N = x - x.mean(axis=0)
fact = np.float(N.shape[0]-1)
cov = np.array(np.dot(N.T, N)/fact)

# print "np.shape(x): {}".format(np.shape(x))
# print "np.shape(R_cancer): {}".format(np.shape(R_cancer))
# print "np.shape(cov): {}".format(np.shape(cov))
# print "\nnp.allclose(cov, R_cancer): {}\n".format(np.allclose(cov, R_cancer))

u, s, vh = np.linalg.svd(cov, full_matrices=False)
# u2, s2, vh2 = np.linalg.svd(cov.T, full_matrices=False)
# u3, s3, vh3 = np.linalg.svd(cov, full_matrices=True)
# u4, s4, vh4 = np.linalg.svd(cov.T, full_matrices=True)

u_x, s_x, vh_x = np.linalg.svd(x, full_matrices=False)
# u_x2, s_x2, vh_x2 = np.linalg.svd(x.T, full_matrices=False)
# u_x3, s_x3, vh_x3 = np.linalg.svd(x, full_matrices=True)
# u_x4, s_x4, vh_x4 = np.linalg.svd(x.T, full_matrices=True)
# print "np.shape(u): {}, np.shape(s): {}, np.shape(vh): {}".format(np.shape(u), np.shape(s), np.shape(vh))
# print "np.shape(u2): {}, np.shape(s2): {}, np.shape(vh2): {}".format(np.shape(u2), np.shape(s2), np.shape(vh2))
# print "np.shape(u3): {}, np.shape(s3): {}, np.shape(vh3): {}".format(np.shape(u3), np.shape(s3), np.shape(vh3))
# print "np.shape(u4): {}, np.shape(s4): {}, np.shape(vh4): {}".format(np.shape(u4), np.shape(s4), np.shape(vh4))
# print "np.shape(u_x): {}, np.shape(s_x): {}, np.shape(vh_x): {}".format(np.shape(u_x), np.shape(s_x), np.shape(vh_x))
# print "np.shape(u_x2): {}, np.shape(s_x2): {}, np.shape(vh_x2): {}".format(np.shape(u_x2), np.shape(s_x2), np.shape(vh_x2))
# print "np.shape(u_x3): {}, np.shape(s_x3): {}, np.shape(vh_x3): {}".format(np.shape(u_x3), np.shape(s_x3), np.shape(vh_x3))
# print "np.shape(u_x4): {}, np.shape(s_x4): {}, np.shape(vh_x4): {}".format(np.shape(u_x4), np.shape(s_x4), np.shape(vh_x4))
temp = [np.random.uniform(0, 0*0.7) for j in xrange(len(s))]  # math.sqrt
evals = np.array((s + temp*s)**0.5)*math.sqrt(fact)
x2 = np.array(np.dot(u_x* evals[:len(s_x)], vh_x))
print "\nnp.allclose(x2, x): {}\n".format(np.allclose(x2, x))

plt.ylim(np.min(x[0]), np.max(x[0]))
plt.bar(np.array(xrange(len(x[0]))), x[0], color='r')
plt.savefig('x[0].png', dpi=300)
plt.show()


plt.ylim(np.min(x2[0]), np.max(x2[0]))
plt.bar(np.array(xrange(len(x2[0]))), x2[0], color='c')
plt.savefig('x2[0].png', dpi=300)
plt.show()


print "finishing successfully"
