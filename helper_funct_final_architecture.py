from pylab import *
from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.models import model_from_config
from tensorflow.python.keras.optimizers import RMSprop
from tensorflow.python.keras.callbacks import Callback
from tensorflow import keras
from sklearn import preprocessing
from sklearn.utils import shuffle
from numpy import linalg as LA
import random
import csv
import os
from sklearn.decomposition import PCA
import itertools
import math
import h5py

class ResetStatesCallback(Callback):
    def __init__(self):
        self.counter = 0

    def on_batch_begin(self, batch, logs={}):
        if self.counter % 32 == 0:
            self.model.reset_states()
        self.counter += 1

class EarlyStoppingByLossVal(Callback):

    def __init__(self, monitor='loss', value=0.001, verbose=1):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn("Early stopping requires %s available!" % self.monitor, RuntimeWarning)
        else:
            if (current < self.value) or (current > 3 and epoch > 15):
                if self.verbose > 0:
                    print "\n----- Stopping at Acc: ", logs.get('acc'), " Loss: ", logs.get('loss'), " -----\n"
                self.model.stop_training = True


class EarlyStoppingByLossVal2(Callback):

    def __init__(self, monitor='acc', value=0.95, verbose=1):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn("Early stopping requires %s available!" % self.monitor, RuntimeWarning)
        else:
            if current > self.value or logs.get('loss') < 0.1:
                if self.verbose > 0:
                    print "\n----- Stopping at Acc: ", logs.get('acc'), " Loss: ", logs.get('loss'), " -----\n"
                self.model.stop_training = True


class EarlyStoppingByLossVal3(Callback):

    def __init__(self, monitor='loss', value=0.001, verbose=1):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn("Early stopping requires %s available!" % self.monitor, RuntimeWarning)
        else:
            if current < self.value:
                if self.verbose > 0:
                    print "\n----- Stopping at Acc: ", logs.get('acc'), " Loss: ", logs.get('loss'), " -----\n"
                self.model.stop_training = True

def normalize_data(x):


    for i in xrange(x.shape[1]):
        x_mean = np.mean(x[:, i])
        x[:, i] = x[:, i] - x_mean
    del x_mean

    # for i in xrange(x.shape[0]):
    #     x_sum = np.sum(x[i])
    #     x[i] = x[i] / x_sum
    #
    # del x_sum

    return x



def read_data(n_folds, fancyPCA, fancy_PCA_add, verbose):
    x_train = [[] for _ in xrange(n_folds)]
    x_test = [[] for _ in xrange(n_folds)]
    y_train = [[] for _ in xrange(n_folds)]
    y_test = [[] for _ in xrange(n_folds)]
    x_val = [[] for _ in xrange(n_folds)]
    y_val = [[] for _ in xrange(n_folds)]

    # Naming convention of the files
    start_train = "_train["
    start_test = "_test["
    start_val = "_val["
    file_type_end = "].csv"

    path = "data/"
    for i in range(0, n_folds):
        # i = i + 1
        if verbose >0:
            print "Loading fold: {}/4".format(i+1)
        csv_x_train = path + "x" + start_train + str(i) + file_type_end
        csv_y_train = path + "y" + start_train + str(i) + file_type_end
        csv_x_test = path + "x" + start_test + str(i) + file_type_end
        csv_y_test = path + "y" + start_test + str(i) + file_type_end
        csv_x_val = path + "x" + start_val + str(i) + file_type_end
        csv_y_val = path + "y" + start_val + str(i) + file_type_end
        # i = i - 1
        x_train[i] = normalize_data(np.array(list(csv.reader(open(csv_x_train)))).astype(np.float))
        y_train[i] = np.array(list(csv.reader(open(csv_y_train)))).astype(int)
        x_test[i] = normalize_data(np.array(list(csv.reader(open(csv_x_test)))).astype(np.float))
        y_test[i] = np.array(list(csv.reader(open(csv_y_test)))).astype(int)
        x_val[i] = normalize_data(np.array(list(csv.reader(open(csv_x_val)))).astype(np.float))
        y_val[i] = np.array(list(csv.reader(open(csv_y_val)))).astype(int)


        # Fancy PCA
        if fancyPCA and fancy_PCA_add > 0:
            print "doing Facny PCA"
            if verbose>0:
                print "adding fancy_PCA_add: {}".format(fancy_PCA_add)
                print "x_train[i][0,:10]: {}".format(x_train[i][0,:10])
            min_max_scaler_cancer = preprocessing.MinMaxScaler()
            min_max_scaler_healthy = preprocessing.MinMaxScaler()
            idx_cancer = np.where(np.array(y_train[i] == [1, 0]).astype(int)[:,0])[0]
            idx_healthy = np.where(np.array(y_train[i] == [0, 1]).astype(int)[:,0])[0]
            x_cancer = x_train[i][idx_cancer]
            x_healthy = x_train[i][idx_healthy]
            # print "np.shape(idx_cancer): {}\n".format(np.shape(idx_cancer))
            # print "np.shape(idx_healthy): {}\n".format(np.shape(idx_healthy))
            # print "np.shape(y_train[i]): {}\n".format(np.shape(y_train[i]))
            # if verbose > 0:
            #     print "np.shape(x_cancer): {}\n".format(np.shape(x_cancer))
            #     print "np.shape(x_healthy): {}\n".format(np.shape(x_healthy))
            #
            # print "y_train[i][:120]: {}\n".format(y_train[i][:120])
            # print "idx_cancer[:120]: {}\n".format(idx_cancer[:120])
            # print "idx_healthy[:120]: {}\n".format(idx_healthy[:120])
            #
            # print np.shape(x_cancer)
            # print np.shape(x_healthy)
            # x_train_new = np.zeros(np.shape(x_train[i]))
            # x_train_new[idx_cancer] = x_cancer
            # x_train_new[idx_healthy] = x_healthy
            # print "Same?: {}".format(np.allclose(x_train[i], x_train_new))

            scaled_data_cancer = min_max_scaler_cancer.fit_transform(x_cancer)
            scaled_data_healthy = min_max_scaler_healthy.fit_transform(x_healthy)
            # print "np.shape(scaled_data_cancer): {}".format(np.shape(scaled_data_cancer))
            # # print "np.shape(scaled_data_healthy): {}".format(np.shape(scaled_data_healthy))
            # m_cancer = np.zeros((1, scaled_data_cancer.shape[0]))[0]
            # m_healthy = np.zeros((1,scaled_data_healthy.shape[0]))[0]
            # # print "np.shape(m_cancer): {}".format(np.shape(m_cancer))
            # print "np.shape(m_healthy): {}".format(np.shape(m_healthy))
            
            # for j in xrange(scaled_data_cancer.shape[0]):
            #     m_cancer[j] = np.mean(scaled_data_cancer[j])
            #     scaled_data_cancer[j] = scaled_data_cancer[j] - m_cancer[j]
            # 
            # for j in xrange(scaled_data_healthy.shape[0]):
            #     m_healthy[j] = np.mean(scaled_data_healthy[j])
            #     scaled_data_healthy[j] = scaled_data_healthy[j] - m_healthy[j]
            m_cancer = scaled_data_cancer.mean(axis=0)   
            m_healthy = scaled_data_healthy.mean(axis=0)
            scaled_data_cancer = scaled_data_cancer - m_cancer
            scaled_data_healthy = scaled_data_healthy - m_healthy
            
            R_cancer = np.cov(scaled_data_cancer, rowvar=False)
            R_healthy = np.cov(scaled_data_healthy, rowvar=False)
            print ("before svd\n")
            u, s, vh = np.linalg.svd(R_cancer, full_matrices=False)
            print ("1/4 svd\n")
            u2, s2, vh2 = np.linalg.svd(R_healthy, full_matrices=False)
            print ("2/4 svd\n")
            u_x, s_x, vh_x = np.linalg.svd(scaled_data_cancer, full_matrices=False)
            print ("3/4 svd\n")
            u_x2, s_x2, vh_x2 = np.linalg.svd(scaled_data_healthy, full_matrices=False)
            fact_cancer = np.float(scaled_data_cancer.shape[0] - 1)
            fact_healthy = np.float(scaled_data_healthy.shape[0] - 1)
            print ("after svd\n")

            # print "np.shape(u): {}, np.shape(s): {}, np.shape(sh): {}".format(np.shape(u), np.shape(s), np.shape(vh))
            # print "np.shape(u2): {}, np.shape(s2): {}, np.shape(sh2: {}".format(np.shape(u2), np.shape(s2), np.shape(vh2))
            shape_x_train = np.shape(x_train[i]) 
            # x_train_new = np.zeros(np.shape(x_train[i]))
            y_train_add = np.zeros(np.shape(y_train[i])) + y_train[i]
            for k in xrange(fancy_PCA_add):
                x_train_new = np.zeros(shape_x_train)
                f = 0.7
                f2 = 0
                temp = np.array([np.random.uniform(f2, 3*math.sqrt(s[j]))for j in xrange(len(s))]) #  np.random.uniform(f2, f)
                temp2 = np.array([np.random.uniform(f2, 3*math.sqrt(s2[j])) for j in xrange(len(s2))])
                # print np.shape(s)
                # print np.shape(temp)
                evals = np.array((s + temp*s)**0.5)*math.sqrt(fact_cancer) #+ temp*s
                evals2 = np.array((s2 + temp*s)**0.5)*math.sqrt(fact_healthy)
                # print s
                # print s_x
                # print evals
                # print np.shape(scaled_data_healthy.T)
                # np.dot(u[:, :6] * s, vh)
                # print np.shape(np.array(np.dot(u[:,:len(evals)] * evals, vh)))
                new_scaled_data_cancer = np.array(np.dot(u_x* evals[:len(s_x)], vh_x)) #evals[:len(s_x)]
                # print "are you ready_ new_scaled_data_cancer?: {}".format(np.allclose(new_scaled_data_cancer, scaled_data_cancer))
                new_scaled_data_healthy = np.array(np.dot(u_x2* evals2[:len(s_x2)], vh_x2))
                # print "are you ready_ new_scaled_data_healthy?: {}".format(np.allclose(new_scaled_data_healthy, scaled_data_healthy))
                
                new_scaled_data_cancer = new_scaled_data_cancer + m_cancer
                new_scaled_data_healthy = new_scaled_data_healthy + m_healthy
                
                # for j in xrange(new_scaled_data_cancer.shape[0]):
                #     new_scaled_data_cancer[j] = new_scaled_data_cancer[j] + m_cancer[j]
                # for j in xrange(new_scaled_data_healthy.shape[0]):
                #     new_scaled_data_healthy[j] = new_scaled_data_healthy[j] + m_healthy[j]
                
                fancy_PCA_data_cancer = np.array(min_max_scaler_cancer.inverse_transform(new_scaled_data_cancer)) #[:,:len(evals)]
                fancy_PCA_data_healthy = np.array(min_max_scaler_healthy.inverse_transform(new_scaled_data_healthy))

                x_train_new[idx_cancer] = fancy_PCA_data_cancer
                x_train_new[idx_healthy] = fancy_PCA_data_healthy
                if k == 0:
                    print "are you ready(allclose(x_train[i], x_train_new))?: {}".format(np.allclose(x_train[i], x_train_new))
                    # print "x_train_new.shape: {}".format(x_train_new.shape)
                    # print "x_train[i].shape: {}".format(x_train[i].shape)
                    # f = h5py.File('x2.h5', 'w')
                    # f.create_dataset("x2", data=x_train_new)
                    # f = h5py.File('x.h5', 'w')
                    # f.create_dataset("x", data=x_train[i])
                    # del f

                    # plt.ylim(np.min(x_train[i][0]), np.max(x_train[i][0]))
                    # #plt.bar(np.array(xrange(len(x_train[i][0]))), x_train[i][0], color='r')
                    # #plt.savefig('x_train[i][0].png', dpi=300)
                    # plt.bar(np.array(xrange(len(x_train_new[0]))), x_train_new[0], color='c')
                    # plt.ylim(-0.001, 0.001)
                    # plt.bar(np.array(xrange(len(x_train[i][0]))), x_train[i][0], color='r')
                    # plt.legend([x_train_new[0], x_train[i][0]], ['new sequence', 'old sequence'])
                    # plt.savefig('f_{}_f2_{}_x_train[0][0]_x_train_new.png'.format(f,f2), dpi=300)
                    # #plt.show()


                print "Before: x_train[i][-1, :10]: {}".format(x_train[i][-1, :10])
                x_train[i] = np.array(list(itertools.chain(x_train[i], x_train_new)))
                y_train[i] = np.array(list(itertools.chain(y_train[i], y_train_add)))
                print "After: x_train[i][-1, :10]: {}".format(x_train[i][-1, :10])
                print "x_train_new[-1, :10]: {}".format(x_train_new[-1, :10])

                print "x_train[i][0,:10]: {}".format(x_train[i][0, :10])
                print "x_train_new[0,:10]: {}".format(x_train_new[0, :10])

                print "np.shape(x_train[i][0]): {}".format(np.shape(x_train[i][0]))
                print "np.shape(x_train_new[0]): {}".format(np.shape(x_train_new[0]))
                print "\nwriting to csv:"
                np.savetxt("x_train[i][0].csv", x_train[i][0], delimiter=",")
                np.savetxt("x_train_new[0].csv", x_train_new[0], delimiter=",")



        # ind = np.random.rand(x_train[i].shape[0]).argsort()
        # # print "ind: {}".format(ind)
        # x_train[i] = x_train[i][ind]
        # y_train[i] = y_train[i][ind]



    return x_train, x_test, x_val, y_val, y_train, y_test


def plot_hist(history, i):
    plt.plot(history.history['loss'])
    # plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train'+str(i)], loc='upper left')
    # plt.show()
    savefig('loss_function_' + str(i) + 'fold.png')


def clone_model(model, isloaded, layers_to_load, lstm_type, unit, set, loss_function, optimizer, metrics, custom_objects={}):
    config = {
        'class_name': model.__class__.__name__,
        'config': model.get_config(),
    }
    clone = model_from_config(config, custom_objects=custom_objects)
    clone.compile(loss=loss_function, optimizer=optimizer, metrics=metrics)
    if isloaded:
        path = "final_models_saved/k_Weights_layers={}_Type={}_units={}_Set={}.h5".format(layers_to_load, lstm_type, unit, set)
        print "\n\ncloning weights: {}\n\n".format(path)
        clone.load_weights(path, by_name=True)
        # clone.set_weights(model_recovering.get_weights())
    #for layer in clone.layers:
        #print(layer, layer.trainable)
        #print(layer.get_weights())
        
    return clone


def find_balanced_accuracy(predicted_labels, y_test, verbose):
    from sklearn import metrics
    a = np.array([predicted_labels[:, 0]]).transpose()
    b = np.array([predicted_labels[:, 1]]).transpose()
    a = np.array(a >= b).astype(int)
    b = np.array(abs(a - 1))
    y_pred = np.concatenate((a, b), axis=1)

    # Calculating the balanced accuracy
    TP = sum(((y_pred == [1, 0])[:, 0]) & (((y_pred == y_test).astype(int))[:, 0])) * 1.0
    TN = sum(((y_pred == [0, 1])[:, 0]) & (((y_pred == y_test).astype(int))[:, 0])) * 1.0
    P = sum(((y_test == [1, 0]).astype(int))[:, 0]) * 1.0
    N = sum(((y_test == [0, 1]).astype(int))[:, 0]) * 1.0
    bal_accuracy = (TP / P + TN / N) / 2.0

    FP = sum(((y_pred == [1, 0])[:, 0]) & (((y_pred != y_test).astype(int))[:, 0])) * 1.0
    FN = sum(((y_pred == [0, 1])[:, 0]) & (((y_pred != y_test).astype(int))[:, 0])) * 1.0
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    F1_score = 2 * (precision * recall) / (precision + recall)

    auc2 = metrics.roc_auc_score(y_test[:,0], y_pred[:,0])
    if (verbose > 0):
        print("TP:", TP)
        print("TN:", TN)
        print("P:", P)
        print("N:", N)
        print "\nFP: {}".format(FP)
        print "FN: {}".format(FN)
        print "F1_score: {}".format(F1_score)
        print "bal_accuracy: ", bal_accuracy
        print "auc2: {}".format(auc2)

    return bal_accuracy


def train_model(x_train, y_train, x_val, y_val, epoch_train, mini_batch_size, shuffle, stateful,
                x_test, y_test, model, n_folds, learning_rate, optimizer, verbose,
                do_save_model, loss_function, metrics, isloaded, n_layers, layers_to_load, type, unit, n_callbacks):


    bal_accuracy = [[0 for _ in xrange(n_folds)] for _ in xrange(n_callbacks)]
    all_test_accuracies = [[0 for _ in xrange(n_folds)] for _ in xrange(n_callbacks)]
    models_weights = [[0 for _ in xrange(n_folds)] for _ in xrange(n_callbacks)]
    predicted_labels = [0 for _ in xrange(n_folds)]
    models_to_save = [0 for _ in xrange(n_folds)]
    callback = [0 for _ in xrange(n_callbacks)]
    models = [Sequential() for _ in xrange(n_folds)]
    history = [0 for _ in xrange(n_folds)]

    for i in xrange(n_folds):
            models[i] = clone_model(model, isloaded, layers_to_load, type, unit, i, loss_function, optimizer, metrics)

    #model1 = clone_model(model, isloaded, layers_to_load, type, unit, 0, loss_function, optimizer, metrics)
    # if n_folds > 1:
        # model2 = clone_model(model, isloaded, layers_to_load, type, unit, 1, loss_function, optimizer, metrics)
        # model3 = clone_model(model, isloaded, layers_to_load, type, unit, 2, loss_function, optimizer, metrics)
        # model4 = clone_model(model, isloaded, layers_to_load, type, unit, 3, loss_function, optimizer, metrics)

    tbCallBack = keras.callbacks.TensorBoard(log_dir='./Graph', histogram_freq=0, write_graph=True, write_images=True)
    EarlyStopping = EarlyStoppingByLossVal(monitor='loss', value=0.001, verbose=verbose)
    EarlyStopping2 = keras.callbacks.EarlyStopping(monitor='loss', min_delta=0.0005, patience=10, verbose=verbose, mode='min')
    EarlyStopping4 = keras.callbacks.EarlyStopping(monitor='loss', min_delta=0.0001, patience=10, verbose=verbose, mode='min')

    search_space = np.array(list(reversed(np.linspace(0.000001, 0.15,  n_callbacks))))
    # print search_space
    for k in xrange(len(search_space)):
        # print "K: {} and search_space[k]: {}".format(k, search_space[k])
        callback[k] = [EarlyStoppingByLossVal3(monitor='loss', value=search_space[k], verbose=verbose), EarlyStopping2]

    for callb in callback:
        try:
            for i in xrange(n_folds):
                if verbose > 0:
                    print "Training {}/{} fold for callback: {}/{}".format(i+1, n_folds, callback.index(callb)+1, len(callback))

                models[i].fit(x_train[i], y_train[i], epochs=epoch_train, batch_size=mini_batch_size, callbacks=callb, verbose=verbose, shuffle=shuffle)

        except (KeyboardInterrupt, SystemExit):
            print "\nKeyboardInterrupt error while training\n"

        for i in xrange(n_folds):
            models_weights[callback.index(callb)][i] = np.array(models[i].get_weights())
            bal_accuracy[callback.index(callb)][i] = find_balanced_accuracy(models[i].predict(x_val[i]), y_val[i], verbose)
            # print model1.evaluate(x_val[i], y_val[i], verbose=0)
            all_test_accuracies[callback.index(callb)][i] = find_balanced_accuracy(models[i].predict(x_test[i]), y_test[i], verbose)
            # print model1.evaluate(x_test[i], y_test[i], verbose=0)
            # print "bal_accuracy[{}]: {}".format(i, bal_accuracy[i])

    # max_acc = [-1 for _ in xrange(n_folds)]
    # deep_fit_range = 50
    # for j in xrange(deep_fit_range):
    #     for i in xrange(n_folds):
    #         if verbose > 0:
    #             print "training further for [{}/{}] deeper model fitting: {}/{}".format(i, n_folds, j, deep_fit_range)
    #         models[i].fit(x_train[i], y_train[i], epochs=1, batch_size=mini_batch_size, callbacks=[],
    #                   verbose=0, shuffle=shuffle)
    #         bal_now = find_balanced_accuracy(models[i].predict(x_val[i]), y_val[i], 0)
    #         print "bal_now: {}".format(bal_now)
    #         if max_acc[i] < bal_now:
    #             max_acc[i] = bal_now
    #             models_weights[n_callbacks-1][i] = np.array(models[i].get_weights())
    #             all_test_accuracies[n_callbacks-1][i] = find_balanced_accuracy(models[i].predict(x_test[i]),
    #                                                                                    y_test[i],
    #                                                                                    verbose)


    print "\nbal_accuracy: \n", bal_accuracy, "\n"

    print "\n\nTest accuracies: \n", all_test_accuracies, "\n"

    for i in xrange(n_folds):
        temp = [row[i] for row in bal_accuracy]
        index = temp.index(np.max(temp))
        models_to_save[i] = clone_model(model, isloaded, layers_to_load, type, unit, i, loss_function, optimizer, metrics)
        models_to_save[i].set_weights(models_weights[index][i])
        if verbose > 0:
            print "saving model index: {}".format(index)
            print "saving model set: {}".format(i)

    balanced_accuracy = [0 for _ in xrange(n_folds)]
    for i in xrange(n_folds):
        balanced_accuracy[i] = find_balanced_accuracy(models_to_save[i].predict(x_test[i]), y_test[i], 2)


    average_balanced_accuracy = np.median(balanced_accuracy)

    if do_save_model:
        for i in xrange(n_folds):
            path_model = "final_models_saved/k_Models_layers={}_Type={}_units={}_Set={}.h5".format(n_layers, type, unit, i)
            path_weight = "final_models_saved/k_Weights_layers={}_Type={}_units={}_Set={}.h5".format(n_layers, type, unit, i)

            if os.path.isfile(path_model):
                os.remove(path_model)
                os.remove(path_weight)
            if verbose > 0:
                print "Saving: {}".format(path_model)
                print "Saving: {}".format(path_weight)
            models_to_save[i].save(path_model)
            models_to_save[i].save_weights(path_weight)

    print "\nMedian balanced_accuracy: {}".format(average_balanced_accuracy)
    print "Max possible balanced accuarcy: {}\n".format(max(all_test_accuracies))

    return average_balanced_accuracy
