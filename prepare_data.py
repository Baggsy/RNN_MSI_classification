
# coding: utf-8

# In[1]:


from pylab import *
import random
import csv

n_sets = 8
n_folds = 4
batch_size = 32
min_val_size = batch_size*8

x = [[] for _ in xrange(n_sets)]
y = [[] for _ in xrange(n_sets)]
x_train = [[] for _ in xrange(n_folds)]
x_val = [[] for _ in xrange(n_folds)]
x_test = [[] for _ in xrange(n_folds)]
y_train = [[] for _ in xrange(n_folds)]
y_val = [[] for _ in xrange(n_folds)]
y_test = [[] for _ in xrange(n_folds)]

# Naming convention of the files
file_str_data = "backup/csv_training_cells_"
file_type = ".csv"
file_str_truth = "backup/csv_truth_cells_"      


# In[2]:


# Reading the data files
for i in range(0, n_sets):
    file_name_data = file_str_data + str(i+1) + file_type
    file_name_truth = file_str_truth + str(i+1) + file_type
    x[i] = np.array(list(csv.reader(open(file_name_data))))
    y[i] = np.array(list(csv.reader(open(file_name_truth))))

# Onehotencoding formatting
y_OneHotEncoded = [[] for _ in xrange(n_sets)]
for i in range(0, n_sets):
    y_OneHotEncoded[i] = zeros([len(y[i]), 2])
    a = np.array(y[i] == '1').astype(int)
    b = np.array(y[i] == '2').astype(int)
    y_OneHotEncoded[i] = np.concatenate((a, b), axis=1)



# In[3]:


# 1: Training: L2, L3, L5, L6, L7, L8
#    Test: L1, L4
# x_train[0] = np.concatenate((x[1], x[2], x[4], x[5], x[6], x[7]), axis=0)
# x_test[0] = np.concatenate((x[0], x[3]), axis=0)
# y_train[0] = np.concatenate((y_OneHotEncoded[1], y_OneHotEncoded[2], y_OneHotEncoded[4], y_OneHotEncoded[5], y_OneHotEncoded[6], y_OneHotEncoded[7]), axis=0)
# y_test[0] = np.concatenate((y_OneHotEncoded[0], y_OneHotEncoded[3]), axis=0)
#
# # 2: Training: L1, L3, L4, L5, L6, L8
# #    Test: L2, L7
# x_train[1] = np.concatenate((x[0], x[2], x[3], x[4], x[5], x[7]), axis=0)
# x_test[1] = np.concatenate((x[1], x[6]), axis=0)
# y_train[1] = np.concatenate((y_OneHotEncoded[0], y_OneHotEncoded[2], y_OneHotEncoded[3], y_OneHotEncoded[4], y_OneHotEncoded[5], y_OneHotEncoded[7]), axis=0)
# y_test[1] = np.concatenate((y_OneHotEncoded[1], y_OneHotEncoded[6]), axis=0)
#
# # 3: Training: L1, L2, L3, L4, L5, L7
# #    Test: L6, L8
# x_train[2] = np.concatenate((x[0], x[1], x[2], x[3], x[4], x[6]), axis=0)
# x_test[2] = np.concatenate((x[5], x[7]), axis=0)
# y_train[2] = np.concatenate((y_OneHotEncoded[0], y_OneHotEncoded[1], y_OneHotEncoded[2], y_OneHotEncoded[3], y_OneHotEncoded[4], y_OneHotEncoded[6]), axis=0)
# y_test[2] = np.concatenate((y_OneHotEncoded[5], y_OneHotEncoded[7]), axis=0)
#
# # 4: Training: L1, L2, L4, L6, L7, L8
# #    Test: L3, L5
# x_train[3] = np.concatenate((x[0], x[1], x[3], x[5], x[6], x[7]), axis=0)
# x_test[3] = np.concatenate((x[2], x[4]), axis=0)
# y_train[3] = np.concatenate((y_OneHotEncoded[0], y_OneHotEncoded[1], y_OneHotEncoded[3], y_OneHotEncoded[5], y_OneHotEncoded[6], y_OneHotEncoded[7]), axis=0)
# y_test[3] = np.concatenate((y_OneHotEncoded[2], y_OneHotEncoded[4]), axis=0)
#
# #for i in xrange(n_folds):
#     #n_indx = len(x_train[i]) % batch_size
#     #indixes = random.sample(xrange(len(x_train[i])), min_val_size+n_indx)
#     #x_val[i] = x_train[i][indixes]
#     #y_val[i] = y_train[i][indixes]
#     #x_train[i] = np.delete(x_train[i], indixes, 0)
#     #y_train[i] = np.delete(y_train[i], indixes, 0)
#
#     #n_indx = len(x_test[i]) % batch_size
#     #indixes = random.sample(xrange(len(x_test[i])), n_indx)
#     #np.append(x_val[i], x_test[i][indixes])
#     #np.append(y_val[i], y_test[i][indixes])
#     #x_test[i] = np.delete(x_test[i], indixes, 0)
#     #y_test[i] = np.delete(y_test[i], indixes, 0)
#
#     #n_indx = len(x_val[i]) % batch_size
#     #indixes = random.sample(xrange(len(x_val[i])), n_indx)
#     #x_val[i] = np.delete(x_val[i], indixes, 0)
#     #y_val[i] = np.delete(y_val[i], indixes, 0)
#
# for i in xrange(n_folds):
#     indixes = random.sample(xrange(len(x_train[i])), min_val_size)
#     x_val[i] = x_train[i][indixes]
#     y_val[i] = y_train[i][indixes]
#     x_train[i] = np.delete(x_train[i], indixes, 0)
#     y_train[i] = np.delete(y_train[i], indixes, 0)



# In[ ]:

#
# 1: Training: L2, L3, L5, L6, L7, L8
#    Test: L1, L4
x_train[0] = np.concatenate((x[2], x[4], x[5], x[6], x[7]), axis=0)
x_test[0] = np.concatenate((x[0], x[3]), axis=0)
y_train[0] = np.concatenate((y_OneHotEncoded[2], y_OneHotEncoded[4], y_OneHotEncoded[5], y_OneHotEncoded[6], y_OneHotEncoded[7]), axis=0)
y_test[0] = np.concatenate((y_OneHotEncoded[0], y_OneHotEncoded[3]), axis=0)
x_val[0] = x[1]
y_val[0] = y_OneHotEncoded[1]

# 2: Training: L1, L3, L4, L5, L6, L8
#    Test: L2, L7
x_train[1] = np.concatenate((x[0], x[2], x[3], x[4], x[5]), axis=0)
x_test[1] = np.concatenate((x[1], x[6]), axis=0)
y_train[1] = np.concatenate((y_OneHotEncoded[0], y_OneHotEncoded[2], y_OneHotEncoded[3], y_OneHotEncoded[4], y_OneHotEncoded[5]), axis=0)
y_test[1] = np.concatenate((y_OneHotEncoded[1], y_OneHotEncoded[6]), axis=0)
x_val[1] = x[7]
y_val[1] = y_OneHotEncoded[7]

# 3: Training: L1, L2, L3, L4, L5, L7
#    Test: L6, L8
x_train[2] = np.concatenate((x[0], x[1], x[2], x[3], x[4]), axis=0)
x_test[2] = np.concatenate((x[5], x[7]), axis=0)
y_train[2] = np.concatenate((y_OneHotEncoded[0], y_OneHotEncoded[1], y_OneHotEncoded[2], y_OneHotEncoded[3], y_OneHotEncoded[4]), axis=0)
y_test[2] = np.concatenate((y_OneHotEncoded[5], y_OneHotEncoded[7]), axis=0)
x_val[2] = x[6]
y_val[2] = y_OneHotEncoded[6]

# 4: Training: L1, L2, L4, L6, L7, L8
#    Test: L3, L5
x_train[3] = np.concatenate((x[1], x[3], x[5], x[6], x[7]), axis=0)
x_test[3] = np.concatenate((x[2], x[4]), axis=0)
y_train[3] = np.concatenate(( y_OneHotEncoded[1], y_OneHotEncoded[3], y_OneHotEncoded[5], y_OneHotEncoded[6], y_OneHotEncoded[7]), axis=0)
y_test[3] = np.concatenate((y_OneHotEncoded[2], y_OneHotEncoded[4]), axis=0)
x_val[3] = x[0]
y_val[3] = y_OneHotEncoded[0]
#
# for i in xrange(n_folds):
#     n_indx = len(x_train[i]) % batch_size
#     indixes = xrange(len(x_train[i]) - n_indx, len(x_train[i]))
#     np.append(x_val[i], x_train[i][indixes])
#     np.append(y_val[i], y_train[i][indixes])
#     x_train[i] = np.delete(x_train[i], indixes, 0)
#     y_train[i] = np.delete(y_train[i], indixes, 0)
#
#     n_indx = len(x_test[i]) % batch_size
#     indixes = xrange(len(x_test[i]) - n_indx, len(x_test[i]))
#     np.append(x_val[i], x_test[i][indixes])
#     np.append(y_val[i], y_test[i][indixes])
#     x_test[i] = np.delete(x_test[i], indixes, 0)
#     y_test[i] = np.delete(y_test[i], indixes, 0)
#
#     n_indx = len(x_val[i]) % batch_size
#     indixes = xrange(len(x_val[i]) - n_indx, len(x_val[i]))
#     x_val[i] = np.delete(x_val[i], indixes, 0)
#     y_val[i] = np.delete(y_val[i], indixes, 0)
#
#





# In[5]:
isokay = True
for i in xrange(n_folds):
    if (len(x_val[i]) % batch_size) + (len(x_train[i]) % batch_size) + (len(x_test[i]) % batch_size) > 0:
        isokay = False

if isokay:
    print "Successfully completed"
else:
    print "Successfully completed, but the data is not into batches of: {}".format(batch_size)
# In[6]:




path = "data/"

for i in range(0, n_folds):

    # writing data to separate files
    with open(path + "x_train[" + str(i) + "].csv", "w+") as my_csv:
        csvWriter = csv.writer(my_csv, delimiter=',')
        csvWriter.writerows(x_train[i])

    with open(path + "y_train[" + str(i) + "].csv", "w+") as my_csv:
        csvWriter = csv.writer(my_csv, delimiter=',')
        csvWriter.writerows(y_train[i])

    with open(path + "x_test[" + str(i) + "].csv", "w+") as my_csv:
        csvWriter = csv.writer(my_csv, delimiter=',')
        csvWriter.writerows(x_test[i])

    with open(path + "y_test[" + str(i) + "].csv", "w+") as my_csv:
        csvWriter = csv.writer(my_csv, delimiter=',')
        csvWriter.writerows(y_test[i])

    with open(path + "x_val[" + str(i) + "].csv", "w+") as my_csv:
        csvWriter = csv.writer(my_csv, delimiter=',')
        csvWriter.writerows(x_val[i])
        
    with open(path + "y_val[" + str(i) + "].csv", "w+") as my_csv:
        csvWriter = csv.writer(my_csv, delimiter=',')
        csvWriter.writerows(y_val[i])

