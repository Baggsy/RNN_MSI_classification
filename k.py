
# coding: utf-8

# In[1]:


#!python

from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.layers import Dense, Activation, LSTM, Bidirectional, Dropout, BatchNormalization, GaussianNoise, GaussianDropout, AlphaDropout
from tensorflow.python.keras import metrics
from helper_funct_final_architecture import *
import time
import os

# TODO: gradient back propagation
#       freeze weights and load them for more layers. Train stack
#       use 100 units to find best method.
#       Camelyon17 images. Use CNN network
#       combine image and MSI data. Image captioning
#       stateful = true, or return sequences in last layer



# In[2]:

# HYPER Parameters
mini_batch_size = 32
embedding_size = 8
learning_rate = 0.005 #.000000001
epoch_train = 100  # maximum repetitions
optimizer_str = "RMSprop"
optimizer = keras.optimizers.RMSprop(lr=learning_rate) # keras.optimizers.Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0, amsgrad=True) #
metrics = ['accuracy', 'mae']
bias_init = 'he_normal'  # It draws samples from a truncated normal distribution centered on 0 with stddev = sqrt(2 / fan_in) where  fan_in is the number of input units in the weight tensor.
kernel_init = 'he_normal'
weight_init = 'he_normal'
verbose = 2
dropout_rate = 0.2
noise_stddev = 0.1
return_sequences = True

doDropout = True
stacking = False
batch_normalization = True
deepBach = False
shuffle = True
use_bias = True
stateful = False
isloaded = False
freeze_wights = False
layers_to_load = 2
fancyPCA = False

# Model parameters
units = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]
layers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15]
lstm_type = ['LSTM', 'Bidirectional']
activation = 'softmax'
loss_function = 'binary_crossentropy'
merge_mode = ['ave', 'concat', 'sum']
f_PCA = [3] #2, 3, 4, 5] # 10, 20, 30, 50]
n_callbacks = 40

if stateful:
    shuffle = False
    
unit = 200
n_layers = 2
type = 'Bidirectional'
merge_mode = 'concat'


file = open("results.txt", "a")
file.write("\n\nmini_batch_size: {} learning_rate: {} optimizer: {}".format(mini_batch_size, learning_rate, optimizer_str))
try:
    # Fancy PCA 
    for fancy_PCA_add in f_PCA: 
		# Data specific parameters

		n_sets = 8
		n_folds = 4
		num_classes = 2
		time_steps = 1


		# In[3]:


		# initialing the output array
		balanced_accuracy = np.zeros((len(units), len(layers), len(lstm_type)))
		run_time2 = np.zeros((len(units), len(layers), len(lstm_type)))

		# Reading the data from the read_data function
		x_train, x_test, x_val, y_val, y_train, y_test = read_data(n_folds, fancyPCA, fancy_PCA_add, verbose)

		# in_shape defines the input shape of the LSTM modules
		in_shape = len(x_train[0][0][0])  # data length variable for the input tensor

		# In[4]:



		start_time = time.time()
		model = Sequential()

		# ___________________________________________________________________________________________________________________


		model.add(Bidirectional(
		LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init,
		 kernel_initializer=kernel_init,
		 recurrent_initializer=weight_init), input_shape=(time_steps, in_shape),
		merge_mode=merge_mode, name="Bid_{}_1".format(merge_mode)))
		model.add(GaussianDropout(0.5))
		model.add(
		BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros',
					   gamma_initializer='ones', moving_mean_initializer='zeros',
					   moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None,
					   beta_constraint=None, gamma_constraint=None))
		model.add(Activation('sigmoid'))

		model.add(Bidirectional(
		LSTM(units=unit, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init,
		 recurrent_initializer=weight_init
		 ), merge_mode=merge_mode, name="Bid_{}_2".format(merge_mode)))
		model.add(GaussianDropout(0.5))
		model.add(
		BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros',
					   gamma_initializer='ones', moving_mean_initializer='zeros',
					   moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None,
					   beta_constraint=None, gamma_constraint=None))
		model.add(Activation('sigmoid'))

		# ___________________________________________________________________________________________________________________


		#model.add(












		# ___________________________________________________________________________________________________________________

		# model.add(Bidirectional(LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init,
		#                                                  recurrent_initializer=weight_init), input_shape=(time_steps, in_shape),
		#                                                  merge_mode=merge_mode, name="Bid_{}_1".format(merge_mode)))
		# # model.add(Bidirectional(
		# #     LSTM(units=unit, stateful=stateful, bias_initializer=bias_init,
		# #          kernel_initializer=kernel_init,
		# #          recurrent_initializer=weight_init), input_shape=(time_steps, in_shape),
		# #     merge_mode=merge_mode, name="Bid_{}_1".format(merge_mode)))
		#
		# # model.add(Bidirectional(LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init,
		# #                              recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_2".format(merge_mode)))
		# model.add(GaussianDropout(0.5))
		# model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros',
		#                              moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
		# model.add(Activation('sigmoid'))
		#
		# # model.add(Bidirectional(
		# #     LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init,
		# #          kernel_initializer=kernel_init,
		# #          recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_3".format(merge_mode)))
		# # model.add(Bidirectional(
		# #     LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init,
		# #          kernel_initializer=kernel_init,
		# #          recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_4".format(merge_mode)))
		# # model.add(GaussianDropout(0.5))
		# # model.add(
		# #     BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros',
		# #                        gamma_initializer='ones', moving_mean_initializer='zeros',
		# #                        moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None,
		# #                        beta_constraint=None, gamma_constraint=None))
		# # model.add(Activation('sigmoid'))
		# #
		# # model.add(Bidirectional(
		# #     LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init,
		# #          kernel_initializer=kernel_init,
		# #          recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_5".format(merge_mode)))
		# # model.add(Bidirectional(
		# #     LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init,
		# #          kernel_initializer=kernel_init,
		# #          recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_2".format(merge_mode)))
		# # model.add(GaussianDropout(0.5))
		# # model.add(
		# #     BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros',
		# #                        gamma_initializer='ones', moving_mean_initializer='zeros',
		# #                        moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None,
		# #                        beta_constraint=None, gamma_constraint=None))
		# # model.add(Activation('sigmoid'))
		#
		# # model.add(Bidirectional(
		# #     LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init,
		# #          kernel_initializer=kernel_init,
		# #          recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_7".format(merge_mode)))
		# model.add(Bidirectional(LSTM(units=unit, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init, recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_2".format(merge_mode)))
		# model.add(GaussianDropout(0.5))
		# model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
		# model.add(Activation('sigmoid'))
		# # model.add(Dense(units=10, name="Dense_layers_1_{}".format(n_layers)))

		# ___________________________________________________________________________________________________________________

		n_layers = 2


		model.add(Dense(units=num_classes, name="Dense_layers_{}".format(n_layers)))
		model.add(Activation(activation=activation))

		model.summary()

		bal_accuracy = train_model(x_train, y_train, x_val, y_val, epoch_train, mini_batch_size, shuffle, stateful,
											x_test, y_test, model, n_folds, learning_rate, optimizer, verbose,
											loss_function, metrics, isloaded, n_layers, layers_to_load, type, unit, n_callbacks)

		# Saving the balanced accuracy over the 4 folds
		balanced_accuracy = bal_accuracy
		file.write(" units: {} n_layers: {} type: {} balanced accuracy: {}\n".format(unit, n_layers, type, bal_accuracy))

		del model


		run_time = time.time() - start_time

		print ""
		print "balanced_accuracy: "
		print balanced_accuracy
		# print ""
		print "Total run time: ", run_time
		file.write("Balanced accuracy: {}".format(balanced_accuracy))
		file.write("times: {}\n\n".format(run_time2))
except (KeyboardInterrupt, SystemExit):
        print "\nKeyboardInterrupt error\n"
        raise
except Exception as e:
    raise
    file.write("\nError: {}\n\n".format(str(e)))
    print "\nError: {}\n\n".format(str(e))

file.write("---------------------\n\n".format(str(e)))
file.close()

