clear all; close all; clc;
tic
%% Script originally taken from:
% article "Deep Learning for Tumor Classification in Imaging Mass Spectrometry" by
% Jens Behrmann, Christian Etmann, Tobias Boskamp, Rita Casadonte, J�rg Kriegsmann, Peter Maass

% Modified by Andreas Panteli, TU/e. a.panteli@student.tue.nl

%% Script to train an IsotopeNet on Task ADSQ and Task LP
% Steps:
% 1.) Specification of dataset 
%       (Note: Path has to be adjusted)
% 2.) Specification: CNN-architecture file and training parameters
%       (Note: Path has to be adjusted)
% 3.) Training on Task ADSQ
% 4.) Training on Task LP


path = '../../fabulous_MSI/RNN_approach/Baselines/Deep_Learning_for_Tumor_Classification_in_IMS/MSClassifyLib';
addpath(strcat(path,'/Classification'));
addpath(strcat(path,'/ClassificationValidation'));
addpath(strcat(path,'/Core'));
addpath(strcat(path,'/DataLoaders'));
addpath(strcat(path,'/FeatureExtraction'));
addpath(strcat(path,'/Helpers'));
addpath(strcat(path,'/Preprocessing'));



%% Load data
% Change path to saving folder of dataset
ADSQ = load('DataTaskADSQ.mat');
type = "ADSQ";
% LP = load('../DataTaskLP.mat');
% type = "LP";


%% Preprocessing

ADSQ.trainingData.initNormalization;
ADSQ.trainingData.setNormalization('tic');
% ADSQ.trainingData.showNormalization;

data = ADSQ.trainingData.data;

ground_truth = ADSQ.trainingPartition.classes.data;
ground_truth_labels = ADSQ.trainingPartition.sampleSets.data;

begin_str_1 = 'backup/csv_training_cells_';
begin_str_2 = 'backup/csv_truth_cells_';
end_str = '.csv';
t1 = cell(1,8);
t2 = cell(1,8);
%% Split into 8 sets + saving to csv files
n_sets = 8; 
for i = 1:n_sets
    full_str_1 = strcat(begin_str_1, num2str(i), end_str);
    full_str_2 = strcat(begin_str_2, num2str(i), end_str);
%     t1{i} = double(data(ground_truth_labels == i,:));
%     t2{i} =  ground_truth(ground_truth_labels == i,:);
    csvwrite(full_str_1, double(data(ground_truth_labels == i,:)));
    csvwrite(full_str_2, ground_truth(ground_truth_labels == i,:));
end


