
# coding: utf-8

# In[1]:


#!python

from tensorflow.python.keras.models import Sequential, load_model
from tensorflow.python.keras.layers import Dense, Activation, LSTM, GRU, Reshape, Bidirectional, Dropout, BatchNormalization, InputLayer, GaussianNoise, GaussianDropout, AlphaDropout
from keras.layers.cudnn_recurrent import CuDNNLSTM, CuDNNGRU
from tensorflow.python.keras import metrics
from helper_funct_final_architecture import *
import time
import os
from keras import regularizers

# TODO: gradient back propagation
#       freeze weights and load them for more layers. Train stack
#       use 100 units to Wfind best method.
#       Camelyon17 images. Use CNN network
#       combine image and MSI data. Image captioning
#       stateful = true, or return sequences in last layer



# In[2]:

# HYPER Parameters
mini_batch_size = 32
embedding_size = 8
learning_rate = 0.0001 # 0.002 # 0.000000001 # 0.005 #
epoch_train = 100  # maximum repetitions
optimizer_str = "RMSprop"
optimizer = keras.optimizers.RMSprop(lr=learning_rate) # keras.optimizers.Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0, amsgrad=True) #
metrics = ['accuracy', 'mae']
bias_init = 'he_normal'  # It draws samples from a truncated normal distribution centered on 0 with stddev = sqrt(2 / fan_in) where  fan_in is the number of input units in the weight tensor.
kernel_init = 'he_normal'
weight_init = 'he_normal'
verbose = 2
dropout_rate = 0.5# 0.83
noise_stddev = 0.1
return_sequences = True
add_dim = True


# Data specific parameters
n_sets = 8
n_folds = 1#4
num_classes = 2

doDropout = False
batch_normalization = False
deepBach = False
shuffle = True
use_bias = True
stateful = False
isloaded = False
stacking = False
freeze_wights = False
do_save_model = False

# Model parameters
units = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]
layers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15]
lstm_type = ['LSTM', 'Bidirectional']
activation = 'softmax'
loss_function = 'binary_crossentropy' # 'mean_absolute_error'
merge_mode = ['ave', 'concat', 'sum']
if stateful:
    shuffle = False
    
units = [100]
unitL1 = 2000
layers = [1,2,3,4]
lstm_type = ['LSTM']
merge_mode = 'ave'
n_callbacks = 30
f_PCA = [0] # 10, 20, 30, 50]
fancyPCA = False
doL1 = False
doRBM = False
n_components_RBM = 200
find_inter_quartile_range = False

file = open("results.txt", "a")
file.write("\n\nmini_batch_size: {} learning_rate: {} optimizer: {}, f_PCA:{}, dropout_rate:{}".format(mini_batch_size, learning_rate, optimizer_str, f_PCA, dropout_rate))
try:
    if find_inter_quartile_range:
        time_step = 1
        i_range = 10
        balanced_accuracy = [[] for _ in xrange(i_range)]
        for inter in xrange(i_range):
            x_train, x_test, x_val, y_val, y_train, y_test = read_data(n_folds, fancyPCA, 0, verbose)
            for i in xrange(n_folds):
                x_train[i] = np.reshape(x_train[i],
                                        (x_train[i].shape[0], time_step, (int(ceil(x_train[i].shape[1] / time_step)))))
                x_test[i] = np.reshape(x_test[i],
                                       (x_test[i].shape[0], time_step, (int(ceil(x_test[i].shape[1] / time_step)))))
                x_val[i] = np.reshape(x_val[i],
                                      (x_val[i].shape[0], time_step, (int(ceil(x_val[i].shape[1] / time_step)))))


            # in_shape defines the input shape of the LSTM modules
            in_shape = len(x_train[0][0][0])  # data length variable for the input tensor
            time_steps = len(x_train[0][0])
            model = Sequential()
            model.add(InputLayer(input_shape=(time_steps, in_shape)))  # input_shape=(27286,)))

            model.add(Dense(100,
                            bias_regularizer=regularizers.l1(1),
                            kernel_regularizer=regularizers.l1(0.0000001),
                            activity_regularizer=regularizers.l1(0.0000001)
                            ))
            model.add(Dense(100,
                            bias_regularizer=regularizers.l1(1),
                            kernel_regularizer=regularizers.l1(0.0000001),
                            activity_regularizer=regularizers.l1(0.0000001)
                            ))

            model.add(LSTM(units=100, stateful=stateful, bias_initializer=bias_init, return_sequences=return_sequences,
                           kernel_initializer=kernel_init, recurrent_initializer=weight_init
                           ))

            model.add(LSTM(units=100, stateful=stateful, bias_initializer=bias_init,
                           kernel_initializer=kernel_init, recurrent_initializer=weight_init
                           ))
            model.add(Dense(units=num_classes))
            model.add(Activation(activation=activation))
            bal_accuracy = train_model(x_train, y_train, x_val, y_val, epoch_train, mini_batch_size, shuffle, stateful,
                                       x_test, y_test, model, n_folds, learning_rate, optimizer, verbose,
                                       do_save_model, loss_function, metrics, isloaded, 4, 0, 'LSTM',
                                       100, n_callbacks)
            balanced_accuracy[inter] = bal_accuracy


            del model

        print balanced_accuracy

    else:
        for fancy_PCA_add in f_PCA:

            # initialing the output array
            balanced_accuracy = np.zeros((len(units), len(layers), len(lstm_type)))
            run_time2 = np.zeros((len(units), len(layers), len(lstm_type)))

            # Reading the data from the read_data function
            time_step = 1
            x_train, x_test, x_val, y_val, y_train, y_test = read_data(n_folds, fancyPCA, fancy_PCA_add, verbose)

            if add_dim:
                for i in xrange(n_folds):
                    # Reshape to tensor input shape for LSTM
                    x_train[i] = np.reshape(x_train[i], (x_train[i].shape[0], time_step, (int(ceil(x_train[i].shape[1]/time_step)))))
                    x_test[i] = np.reshape(x_test[i], (x_test[i].shape[0], time_step, (int(ceil(x_test[i].shape[1]/time_step)))))
                    x_val[i] = np.reshape(x_val[i], (x_val[i].shape[0], time_step, (int(ceil(x_val[i].shape[1]/time_step)))))
                    if verbose > 0:
                        print "\nshape of x_train[{}]: {}.".format(i, np.shape(x_train[i]))
                        print "shape of x_test[{}]: {}".format(i, np.shape(x_test[i]))
                        print "sshape of x_val[{}]: {}".format(i, np.shape(x_val[i]))

                # in_shape defines the input shape of the LSTM modules
                in_shape = len(x_train[0][0][0])  # data length variable for the input tensor
                time_steps = len(x_train[0][0])

                print "time_steps: {}. in_shape = {}".format(time_steps, in_shape)

            # In[4]:

            start_time = time.time()
            for unit in units:
                for n_layers in layers:
                    for type in lstm_type:
                        print "unit: ", unit
                        print "n_layers: ", n_layers
                        print "type: ", type
                        start_time2 = time.time()
                        model = Sequential()
                        # Adding the LSTM layers or the Bidirectional LSTM modules

                        if doRBM:
                            # from sklearn.neural_network import BernoulliRBM
                            # from sklearn import linear_model, metrics
                            # from sklearn.pipeline import Pipeline
                            # modelRBM = BernoulliRBM(learning_rate=learning_rate, n_components=n_components_RBM,
                            #                         verbose=verbose, batch_size=mini_batch_size, n_iter=epoch_train)
                            # logistic = linear_model.LogisticRegression()
                            # # X_RBM = np.array([[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 1]])
                            # classifier = Pipeline(steps=[('rbm', modelRBM), ('logistic', logistic)])
                            # logistic.C = 6000.0
                            # print ("doing Restricted Botlzman Machine training")
                            # # X_RBM = modelRBM.fit_transform(x_train[0])
                            # print "x_train[0]: {}, y_train[0]:  {}".format(np.shape(x_train[0]), np.shape(y_train[0][:,0]))
                            # classifier.fit(x_train[0], y_train[0][:,0])
                            #
                            # # logistic_classifier = linear_model.LogisticRegression(C=100.0)
                            # # logistic_classifier.fit(x_train[0], y_train[0])
                            # print()
                            # print("Logistic regression using RBM features:\n%s\n" % (
                            #     metrics.classification_report(
                            #         y_test[0][:,0],
                            #         clahttps://vunet.login.vu.nl/Pages/modulesubscribe.aspx#/ssifier.predict(x_test[0]))))

                            import pandas as pd
                            import msgpack
                            import glob
                            import tensorflow as tf
                            from tensorflow.python.ops import control_flow_ops
                            from tqdm import tqdm

                            n_visible = 27286
                            n_hidden = 100
                            lr = learning_rate
                            num_epochs = 100
                            batch_size = 27286  # 100 # 32
                            tf.reset_default_graph()

                            if os.path.isfile("project.ckpt.meta"):
                                print "\nloading project.ckpt.meta\n"

                                tf.reset_default_graph()
                                x = tf.placeholder(tf.float32, [None, n_visible], name="x")
                                # The weight matrix that stores the edge weights
                                W = tf.Variable(tf.random_normal([n_visible, n_hidden], 0.01), name="W")
                                # The bias vector for the hidden layer
                                bh = tf.Variable(tf.zeros([1, n_hidden], tf.float32, name="bh"))
                                # The bias vector for the visible layer
                                bv = tf.Variable(tf.zeros([1, n_visible], tf.float32, name="bv"))

                                saver = tf.train.Saver()
                                with tf.Session() as sess:
                                    saver.restore(sess, "project.ckpt.meta")


                            else:


                                for i in xrange(n_folds):
                                    # Reshape to tensor input shape for LSTM
                                    x_train[i] = np.reshape(x_train[i], (x_train[i].shape[0], x_train[i].shape[2]))
                                    x_test[i] = np.reshape(x_test[i], (x_test[i].shape[0], x_test[i].shape[2]))
                                    x_val[i] = np.reshape(x_val[i], (x_val[i].shape[0], x_val[i].shape[2]))
                                    if verbose > 0:
                                        print "\nShape of RBM input:\n"
                                        print "shape of x_train[{}]: {}.".format(i, np.shape(x_train[i]))
                                        print "shape of x_test[{}]: {}".format(i, np.shape(x_test[i]))


                                # The placeholder variable that holds our data
                                x = tf.placeholder(tf.float32, [None, n_visible],name="x")
                                # The weight matrix that stores the edge weights
                                W = tf.Variable(tf.random_normal([n_visible, n_hidden], 0.01),name="W")
                                # The bias vector for the hidden layer
                                bh = tf.Variable(tf.zeros([1, n_hidden], tf.float32, name="bh"))
                                # The bias vector for the visible layer
                                bv = tf.Variable(tf.zeros([1, n_visible], tf.float32, name="bv"))


                                # This function lets us easily sample from a vector of probabilities
                                def sample(probs):
                                    # Takes in a vector of probabilities, and returns a random vector of 0s and 1s sampled from the input vector
                                    return tf.floor(probs + tf.random_uniform(tf.shape(probs), 0, 1))


                                # This function runs the gibbs chain. We will call this function in two places:
                                #    - When we define the training update step
                                #    - When we sample our music segments from the trained RBM
                                def gibbs_sample(k):
                                    # Runs a k-step gibbs chain to sample from the probability distribution of the RBM defined by W, bh, bv
                                    def gibbs_step(count, k, xk):
                                        # Runs a single gibbs step. The visible values are initialized to xk
                                        hk = sample(tf.sigmoid(
                                            tf.matmul(xk, W) + bh))  # Propagate the visible values to sample the hidden values
                                        xk = sample(tf.sigmoid(tf.matmul(hk, tf.transpose(
                                            W)) + bv))  # Propagate the hidden values to sample the visible values
                                        return count + 1, k, xk

                                    # Run gibbs steps for k iterations
                                    ct = tf.constant(0)  # counter

                                    [_, _, x_sample] = control_flow_ops.while_loop(lambda count, num_iter, *args: count < num_iter,
                                                                              gibbs_step, [ct, tf.constant(k), x])
                                    # This is not strictly necessary in this implementation, but if you want to adapt this code to use one of TensorFlow's
                                    # optimizers, you need this in order to stop tensorflow from propagating gradients back through the gibbs step
                                    x_sample = tf.stop_gradient(x_sample)
                                    return x_sample


                                ### Training Update Code
                                # Now we implement the contrastive divergence algorithm. First, we get the samples of x and h from the probability distribution
                                # The sample of x
                                x_sample = gibbs_sample(1)
                                # The sample of the hidden nodes, starting from the visible state of x
                                h = sample(tf.sigmoid(tf.matmul(x, W) + bh))
                                # The sample of the hidden nodes, starting from the visible state of x_sample
                                h_sample = sample(tf.sigmoid(tf.matmul(x_sample, W) + bh))

                                # Next, we update the values of W, bh, and bv, based on the difference between the samples that we drew and the original values
                                size_bt = tf.cast(tf.shape(x)[0], tf.float32)
                                W_adder = tf.multiply(lr / size_bt, tf.subtract(tf.matmul(tf.transpose(x), h),
                                                                      tf.matmul(tf.transpose(x_sample), h_sample)))
                                bv_adder = tf.multiply(lr / size_bt, tf.reduce_sum(tf.subtract(x, x_sample), 0, True))
                                bh_adder = tf.multiply(lr / size_bt, tf.reduce_sum(tf.subtract(h, h_sample), 0, True))
                                # When we do sess.run(updt), TensorFlow will run all 3 update steps
                                updt = [W.assign_add(W_adder), bv.assign_add(bv_adder), bh.assign_add(bh_adder)]

                                saver = tf.train.Saver()

                                with tf.Session() as sess:
                                    # First, we train the model
                                    # initialize the variables of the model
                                    init = tf.initialize_all_variables()
                                    sess.run(init)
                                    # Run through all of the training data num_epochs times
                                    for epoch in tqdm(range(num_epochs)):
                                        for sequence in x_train[0]:
                                            # for i in range(1, len(sequence), batch_size):
                                            #     tr_x = np.transpose(sequence[i:i + batch_size])
                                            #     sess.run(updt, feed_dict={x: tr_x})
                                            tr_x = np.array(sequence).reshape(1, 27286)
                                            sess.run(updt, feed_dict={x: tr_x})

                                    saver.save(sess, "./project.ckpt")

                            exit(0)

                        elif doL1:

                            print ("\ndoing Lasso regression training\n")
                            model.add(InputLayer(input_shape=(time_steps, in_shape))) #input_shape=(27286,)))

                            model.add(Dense(100,
                                            bias_regularizer=regularizers.l1(1),
                                            kernel_regularizer=regularizers.l1(0.0000001),
                                            activity_regularizer=regularizers.l1(0.0000001)
                            ))
                            # model.add(Dropout(0.5, noise_shape=None, seed=None))
                            # model.add(Dense(100,
                            #                 bias_regularizer=regularizers.l1(1),
                            #                 kernel_regularizer=regularizers.l1(0.0000001),
                            #                 activity_regularizer=regularizers.l1(0.0000001)
                            #                 ))
                            # model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                            #                              beta_initializer='zeros', gamma_initializer='ones',
                            #                              moving_mean_initializer='zeros',
                            #                              moving_variance_initializer='ones', beta_regularizer=None,
                            #                              gamma_regularizer=None, beta_constraint=None,
                            #                              gamma_constraint=None))
                            # model.add(Activation('sigmoid'))

                            #
                            model.add(Bidirectional(LSTM(units=100, stateful=stateful, return_sequences=return_sequences, bias_initializer=bias_init,
                                                         kernel_initializer=kernel_init, recurrent_initializer=weight_init
                                                         ), merge_mode=merge_mode,
                                                    name="Bid_{}_1".format(merge_mode, n_layers)))

                            # model.add(Bidirectional(
                            #     LSTM(units=100, stateful=stateful, return_sequences=return_sequences,
                            #          bias_initializer=bias_init,
                            #          kernel_initializer=kernel_init, recurrent_initializer=weight_init
                            #          ), merge_mode=merge_mode,
                            #     name="Bid_{}_2".format(merge_mode, n_layers)))

                            # model.add(LSTM(units=100, stateful=stateful, bias_initializer=bias_init, return_sequences=return_sequences,
                            #                              kernel_initializer=kernel_init, recurrent_initializer=weight_init
                            #                              ))

                            # model.add(Dropout(0.5, noise_shape=None, seed=None))
                            model.add(Bidirectional(LSTM(units=100, stateful=stateful,
                                                         bias_initializer=bias_init,
                                                         kernel_initializer=kernel_init, recurrent_initializer=weight_init
                                                         ), merge_mode=merge_mode,
                                                    name="Bid_{}_2".format(merge_mode, n_layers)))
                            # model.add(Dropout(0.5, noise_shape=None, seed=None))
                            # model.add(Dense(100,
                            #                 bias_regularizer=regularizers.l1(1),
                            #                 kernel_regularizer=regularizers.l1(0.0000001),
                            #                 activity_regularizer=regularizers.l1(0.0000001)
                            #                 ))
                            # if batch_normalization:
                            #     model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                            #                                  beta_initializer='zeros', gamma_initializer='ones',
                            #                                  moving_mean_initializer='zeros',
                            #                                  moving_variance_initializer='ones', beta_regularizer=None,
                            #                                  gamma_regularizer=None, beta_constraint=None,
                            #                                  gamma_constraint=None))
                            #     model.add(Activation('sigmoid'))
                            # if doDropout:
                            #     model.add(Dropout(dropout_rate, noise_shape=None, seed=None))
                            # model.add(Dense(100,
                            #                 bias_regularizer=regularizers.l1(1),
                            #                 kernel_regularizer=regularizers.l1(0.0000001),
                            #                 activity_regularizer=regularizers.l1(0.0000001)
                            #                 ))
                            # model.add(Dropout(dropout_rate, noise_shape=None, seed=None))
                            # model.add(LSTM(units=100, stateful=stateful, bias_initializer=bias_init,
                            #                kernel_initializer=kernel_init, recurrent_initializer=weight_init
                            #                ))
                            # model.add(Dense(10,
                            #                 bias_regularizer=regularizers.l1(1),
                            #                 kernel_regularizer=regularizers.l1(0.0000001),
                            #                 activity_regularizer=regularizers.l1(0.0000001)
                            #                 ))

                            # model.add(Bidirectional(LSTM(units=10, stateful=stateful, bias_initializer=bias_init,
                            #                              kernel_initializer=kernel_init, recurrent_initializer=weight_init
                            #                              ), merge_mode=merge_mode,
                            #                         name="Bid_{}_3".format(merge_mode, n_layers)))
                            #
                            # if batch_normalization:
                            #     model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                            #                                  beta_initializer='zeros', gamma_initializer='ones',
                            #                                  moving_mean_initializer='zeros',
                            #                                  moving_variance_initializer='ones', beta_regularizer=None,
                            #                                  gamma_regularizer=None, beta_constraint=None,
                            #                                  gamma_constraint=None))
                            #     model.add(Activation('sigmoid'))
                            # if doDropout:
                            #     model.add(Dropout(dropout_rate, noise_shape=None, seed=None))


                            # for j in range(2, n_layers):
                                # model.add(Bidirectional(
                                #     LSTM(units=unit, return_sequences=return_sequences, stateful=stateful,
                                #          bias_initializer=bias_init, kernel_initializer=kernel_init,
                                #          recurrent_initializer=weight_init), merge_mode=merge_mode,
                                #     name="Bid_{}_{}".format(merge_mode, j)))
                                # if batch_normalization:
                                #     model.add(
                                #         BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                                #                            beta_initializer='zeros', gamma_initializer='ones',
                                #                            moving_mean_initializer='zeros',
                                #                            moving_variance_initializer='ones', beta_regularizer=None,
                                #                            gamma_regularizer=None, beta_constraint=None,
                                #                            gamma_constraint=None))
                                #     model.add(Activation('sigmoid'))
                                # if doDropout:
                                #     model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                            # model.add(Bidirectional(LSTM(units=10, stateful=stateful, bias_initializer=bias_init,
                            #                              kernel_initializer=kernel_init, recurrent_initializer=weight_init
                            #                              ), merge_mode=merge_mode,
                            #                         name="Bid_{}_{}".format(merge_mode, n_layers)))

                            # if batch_normalization:
                            #     model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                            #                                  beta_initializer='zeros', gamma_initializer='ones',
                            #                                  moving_mean_initializer='zeros',
                            #                                  moving_variance_initializer='ones', beta_regularizer=None,
                            #                                  gamma_regularizer=None, beta_constraint=None,
                            #                                  gamma_constraint=None))
                            #     model.add(Activation('sigmoid'))
                            # if doDropout:
                            #     model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                        else:
                            if type == 'LSTM':
                                if n_layers > 1:
                                    model.add(InputLayer(input_shape=(time_steps, in_shape)))

                                    model.add(LSTM(units=unit, return_sequences=return_sequences, stateful=stateful,
                                                   bias_initializer=bias_init, kernel_initializer=kernel_init,
                                                   recurrent_initializer=weight_init, use_bias=use_bias, name="LSTM_1"))
                                    # if doDropout:
                                    #     model.add(Dropout(0.5, noise_shape=None, seed=None))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                    for j in range(2, n_layers):
                                        model.add(LSTM(units=unit, return_sequences=return_sequences, bias_initializer=bias_init, stateful=stateful,
                                                       kernel_initializer=kernel_init, recurrent_initializer=weight_init,
                                                       use_bias=use_bias, name = "LSTM_{}".format(j)))
                                        # if doDropout:
                                        #     model.add(Dropout(0.5, noise_shape=None, seed=None))
                                        if batch_normalization:
                                            model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                            model.add(Activation('sigmoid'))
                                        if doDropout:
                                            model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                    model.add(LSTM(units=unit, bias_initializer=bias_init, kernel_initializer=kernel_init, stateful=stateful,
                                                   recurrent_initializer=weight_init, use_bias=use_bias, name="LSTM_{}".format(n_layers)))
                                    # if doDropout:
                                    #         model.add(Dropout(0.5, noise_shape=None, seed=None))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                else:
                                    model.add(InputLayer(input_shape=(time_steps, in_shape)))
                                    model.add(LSTM(units=unit, bias_initializer=bias_init, stateful=stateful,
                                                   kernel_initializer=kernel_init, recurrent_initializer=weight_init,
                                                   name="LSTM_1"))
                                    # if doDropout:
                                    #     model.add(Dropout(0.5, noise_shape=None, seed=None))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(0.5, noise_shape=None, seed=None))

                            elif type == 'Bidirectional':
                                if n_layers > 1:
                                    model.add(Bidirectional(LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init,
                                                                     recurrent_initializer=weight_init), input_shape=(time_steps, in_shape),
                                                                merge_mode=merge_mode, name="Bid_{}_1".format(merge_mode)))
                                    # if doDropout:
                                    #     model.add(GaussianDropout(0.2))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                    for j in range(2, n_layers):
                                        model.add(Bidirectional(LSTM(units=unit, return_sequences=return_sequences, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init,
                                                                     recurrent_initializer=weight_init), merge_mode=merge_mode, name="Bid_{}_{}".format(merge_mode, j)))
                                        # if doDropout:
                                        #     model.add(GaussianDropout(0.5))
                                        if batch_normalization:
                                            model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                            model.add(Activation('sigmoid'))
                                        if doDropout:
                                            model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                    model.add(Bidirectional(LSTM(units=unit, stateful=stateful, bias_initializer=bias_init, kernel_initializer=kernel_init, recurrent_initializer=weight_init
                                                                 ), merge_mode=merge_mode, name="Bid_{}_{}".format(merge_mode, n_layers)))
                                    # if doDropout:
                                    #     model.add(GaussianDropout(0.5))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                else:
                                    model.add(Bidirectional(LSTM(units=unit, bias_initializer=bias_init, stateful=stateful, kernel_initializer=kernel_init, recurrent_initializer=weight_init
                                                                 ), input_shape=(time_steps, in_shape), merge_mode=merge_mode, name="Bid_{}_1".format(merge_mode)))
                                    # if doDropout:
                                    #     model.add(GaussianDropout(0.5))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                            else:
                                if n_layers > 1:
                                    model.add(GRU(units=unit, input_shape=(time_steps, in_shape), return_sequences=return_sequences, stateful=stateful,
                                                   bias_initializer=bias_init, kernel_initializer=kernel_init,
                                                   recurrent_initializer=weight_init, use_bias=use_bias, name="GRU_1"))
                                    # if doDropout:
                                    #     model.add(Dropout(0.5, noise_shape=None, seed=None))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                    for j in range(2, n_layers):
                                        model.add(GRU(units=unit, return_sequences=return_sequences, bias_initializer=bias_init, stateful=stateful,
                                                       kernel_initializer=kernel_init, recurrent_initializer=weight_init,
                                                       use_bias=use_bias, name = "GRU_{}".format(j)))
                                        # if doDropout:
                                        #     model.add(Dropout(0.5, noise_shape=None, seed=None))
                                        if batch_normalization:
                                            model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                            model.add(Activation('sigmoid'))
                                        if doDropout:
                                            model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                    model.add(GRU(units=unit, bias_initializer=bias_init, kernel_initializer=kernel_init, stateful=stateful,
                                                   recurrent_initializer=weight_init, use_bias=use_bias, name="GRU_{}".format(n_layers)))
                                    # if doDropout:
                                    #         model.add(Dropout(0.5, noise_shape=None, seed=None))
                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                                else:
                                    model.add(GRU(units=unit, input_shape=(time_steps, in_shape), bias_initializer=bias_init,
                                                   stateful=stateful,
                                                   kernel_initializer=kernel_init, recurrent_initializer=weight_init, name="GRU_1"))

                                    if batch_normalization:
                                        model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
                                        model.add(Activation('sigmoid'))
                                    if doDropout:
                                        model.add(Dropout(0.5, noise_shape=None, seed=None))


                        if stacking:
                            i = n_layers-1
                            layers_to_load = 0
                            isloaded = False
                            while i > 0 and not isloaded:
                                file_name = "final_models_saved/k_Weights_layers={}_Type={}_units={}_Set=0.h5".format(i, type, unit)
                                if os.path.isfile(file_name):
                                    isloaded = True
                                    print "\nWill load {} layers\n".format(i)
                                    layers_to_load = i
                                i -= 1

                            if isloaded and freeze_wights:
                                for layer in model.layers[:i + 1]:
                                    layer.trainable = False
                        else:
                            layers_to_load = n_layers


                        # Adding the rest of the network's components
                        # if doDropout:
                        #     model.add(Dropout(dropout_rate, noise_shape=None, seed=None))

                        # model.add(Dense(units=unit, name="Dense_layers_{}".format(n_layers)))
                        # n_layers = n_layers+1
                        # if batch_normalization:
                        #     model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                        #                                  beta_initializer='zeros', gamma_initializer='ones',
                        #                                  moving_mean_initializer='zeros',
                        #                                  moving_variance_initializer='ones', beta_regularizer=None,
                        #                                  gamma_regularizer=None, beta_constraint=None,
                        #                                  gamma_constraint=None))
                        #     model.add(Activation('sigmoid'))

                        if deepBach:
                            model.add(Dense(units=unit, activation='relu'))

                        model.add(Dense(units=num_classes, name="Dense_layers_{}".format(n_layers)))
                        # model.add(Dense(num_classes,
                        #                 bias_regularizer=regularizers.l1(1),
                        #                 kernel_regularizer=regularizers.l1(0.0000001),
                        #                 activity_regularizer=regularizers.l1(0.0000001),
                        #                 name="Dense_layers_{}".format(n_layers)
                        #                 ))

                        model.add(Activation(activation=activation))
                        # model.compile(loss=loss_function, optimizer=optimizer, metrics=metrics)
                        model.summary()


                        # training of the model
                        bal_accuracy = train_model(x_train, y_train, x_val, y_val, epoch_train, mini_batch_size, shuffle, stateful,
                                                   x_test, y_test, model, n_folds, learning_rate, optimizer, verbose,
                                                   do_save_model, loss_function, metrics, isloaded, n_layers, layers_to_load, type, unit, n_callbacks)

                        # Saving the balanced accuracy over the 4 folds
                        balanced_accuracy[units.index(unit), layers.index(n_layers), lstm_type.index(type)] = bal_accuracy
                        run_time2[units.index(unit), layers.index(n_layers), lstm_type.index(type)] = time.time() - start_time2
                        print "run time of units {} n_layers {} of type {} : {}\n\n---------------------\n\n".format(unit, n_layers, type, run_time2[units.index(unit), layers.index(n_layers), lstm_type.index(type)])

                        file.write(" units: {} n_layers: {} type: {} balanced accuracy: {}\n".format(unit, n_layers, type, bal_accuracy))

                        del model


        run_time = time.time() - start_time

        print ""
        print "balanced_accuracy: "
        print balanced_accuracy
        print "run_time2: "
        print run_time2
        # print ""
        print "Total run time: ", run_time
        file.write("Balanced accuracy: {}".format(balanced_accuracy))
        file.write("times: {}\n\n".format(run_time2))
except (KeyboardInterrupt, SystemExit):
        print "\nKeyboardInterrupt error\n"
        raise
except Exception as e:
    raise
    file.write("\nError: {}\n\n".format(str(e)))
    print "\nError: {}\n\n".format(str(e))

file.write("---------------------\n\n".format(str(e)))
file.close()

